import 'dart:io' show Platform;

import 'package:flutter/cupertino.dart';

abstract class PDWidget<I extends Widget, A extends Widget>
    extends StatelessWidget {
  const PDWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (Platform.isAndroid) {
      return createAndroidWidget(context);
    } else if (Platform.isIOS) {
      return createIosWidget(context);
    }
    // platform not supported returns an empty widget
    return Container();
  }

  I createIosWidget(BuildContext context);

  A createAndroidWidget(BuildContext context);
}
