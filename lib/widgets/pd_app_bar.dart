import 'dart:io' show Platform;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PDAppBar {
  static Widget appBar({Widget? leading, Widget? title}) {
    return Platform.isIOS
        ? CupertinoNavigationBar(
            leading: leading,
            middle: title,
          )
        : AppBar(
            leading: leading,
            title: title,
          );
  }
}
