import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hotdog/widgets/pd_widget.dart';

class PDScaffold extends PDWidget<CupertinoPageScaffold, Scaffold> {

  final Widget body;
  final Widget? appBar;

  const PDScaffold({
    Key? key,
    required this.body,
    this.appBar,
  }) : super(key: key);

  @override
  Scaffold createAndroidWidget(BuildContext context) {
    return Scaffold(
      key: key,
      body: body,
      appBar: appBar as PreferredSizeWidget,
    );
  }

  @override
  CupertinoPageScaffold createIosWidget(BuildContext context) {
    return CupertinoPageScaffold(
      key: key,
      child: body,
      navigationBar: appBar as ObstructingPreferredSizeWidget,
    );
  }
}
