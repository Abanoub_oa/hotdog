import 'dart:io';

abstract class AnyImageClassifier {
  Future<void> start();

  void stop();

  Future<Classification?> classify(File imageFile);
}

abstract class Classification {
  double confidence;
  String label;

  Classification(this.confidence, this.label);
}
