import 'dart:io';

import 'package:hotdog/utils/any_image_classifier.dart';
import 'package:tflite/tflite.dart';

class TFLiteImageClassifier implements AnyImageClassifier {
  @override
  Future<void> start() async {
    await Tflite.loadModel(
        model: "assets/mobnet.tflite", labels: "assets/labels.txt");
  }

  @override
  void stop() {
    Tflite.close();
  }

  @override
  Future<TFLClassification?> classify(File imageFile) async {
    var output = await Tflite.runModelOnImage(path: imageFile.path);

    if (output?.isEmpty == false) {
      final out = output?.first;

      return TFLClassification(out);
    } else {
      return null;
    }
  }
}

class TFLClassification implements Classification {
  @override
  double confidence = 0;

  @override
  String label = "";

  TFLClassification(map) {
    confidence = map["confidence"] as double;
    label = map["label"] as String;
  }
}
