import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:hotdog/utils/any_image_classifier.dart';
import 'package:hotdog/utils/tflite_image_classifier.dart';
import 'package:hotdog/widgets/pd_app_bar.dart';

import 'widgets/pd_scaffold.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({
    Key? key,
    required this.camera,
  }) : super(key: key);

  final CameraDescription camera;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with WidgetsBindingObserver {
  late CameraController _controller;
  late AnyImageClassifier _imageClassifier;

  bool _isLoading = false;
  File? _image;
  bool? _isHotDog;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addObserver(this);

    // To display the current output from the Camera,
    _controller = CameraController(
      widget.camera,
      ResolutionPreset.medium,
    );

    // Next, initialize the controller. This returns a Future.
    _controller.initialize().then((value) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });

    _imageClassifier = TFLiteImageClassifier();
    _imageClassifier.start().then((value) {
      setState(() {});
    });
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);
    // Dispose of the controller when the widget is disposed.
    _controller.dispose();
    _imageClassifier.stop();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // Fill this out in the next steps.
    return PDScaffold(
      appBar: PDAppBar.appBar(
        title: const Text("SeeFood"),
      ),
      body: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 0),
        child: !_controller.value.isInitialized
            ? _getProgressIndicator()
            : Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  _getCameraPreview(),
                  _displayCapturedOutput(),
                  Padding(
                    padding: const EdgeInsets.all(32),
                    child: FloatingActionButton(
                      // Provide an onPressed callback.
                      onPressed: () async {
                        setState(() {
                          _isLoading = true;
                        });

                        if (_image == null) {
                          _capture();
                        } else {
                          _reset();
                        }
                      },
                      child: Icon(_image == null
                          ? Icons.camera_alt
                          : Icons.refresh_outlined),
                      backgroundColor: Colors.red,
                      foregroundColor: Colors.yellow,
                    ),
                  ),
                  _isLoading ? _getProgressIndicator() : const SizedBox.shrink()
                ],
              ),
      ),
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.resumed:
        if (!_controller.value.isInitialized) {
          // Next, initialize the controller. This returns a Future.
          _controller.initialize().then((value) {
            if (!mounted) {
              return;
            }
            setState(() {});
          });
        }
        break;
      default:
        break;
    }
  }

  _capture() async {
    // Take the Picture in a try / catch block. If anything goes wrong,
    // catch the error.
    try {
      // Ensure that the camera is initialized.
      if (!_controller.value.isInitialized) {
        return;
      }

      // Attempt to take a picture and get the file `image`
      // where it was saved.
      final image = await _controller.takePicture();
      _classifyImage(File(image.path));
    } catch (e) {
      // If an error occurs, log the error to the console.
      print(e);
    }
  }

  _reset() async {
    setState(() {
      _isLoading = false;
      _image = null;
      _isHotDog = null;
    });
  }

  Widget _getProgressIndicator() {
    return Center(
      child: CircularProgressIndicator(
        backgroundColor: Colors.black.withOpacity(0.2),
      ),
    );
  }

  Widget _getCameraPreview() {
    final size = MediaQuery.of(context).size;

    var scale = size.aspectRatio * _controller.value.aspectRatio;

    scale = scale < 1 ? 1 / scale : scale;

    return Transform.scale(
      scale: scale,
      child: Center(
        child: CameraPreview(_controller),
      ),
    );
  }

  Widget _displayCapturedOutput() {
    final size = MediaQuery.of(context).size;
    final deviceRatio = size.width / size.height;

    return Builder(builder: (context) {
      return _image == null
          ? const SizedBox.shrink()
          : Padding(
              padding: const EdgeInsets.all(16),
              child: ClipRRect(
                  clipBehavior: Clip.hardEdge,
                  borderRadius: BorderRadius.circular(8),
                  child: Stack(
                    alignment: Alignment.topCenter,
                    fit: StackFit.expand,
                    children: [
                      AspectRatio(
                          aspectRatio: deviceRatio,
                          child: Image.file(
                            _image!,
                            fit: BoxFit.cover,
                          )),
                      Stack(
                        alignment: Alignment.topCenter,
                        fit: StackFit.expand,
                        children: [
                          Positioned(
                            top: 0,
                            left: 0,
                            right: 0,
                            height: 70,
                            child: Container(
                              color: _isHotDog! ? Colors.green : Colors.red,
                              height: 60,
                            ),
                          ),
                          Positioned(
                            top: 38,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(28),
                              child: Container(
                                color: _isHotDog! ? Colors.green : Colors.red,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Image.asset(
                                    _isHotDog!
                                        ? "assets/ic_is_hotdog.png"
                                        : "assets/ic_isnt_hotdog.png",
                                    width: 40,
                                    height: 40,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            top: 0,
                            left: 0,
                            right: 0,
                            height: 60,
                            child: SizedBox(
                              height: 60,
                              child: Center(
                                child: Text(
                                    _isHotDog! ? "Hotdog!" : "Not hotdog!",
                                    style: const TextStyle(
                                        color: Colors.yellow,
                                        fontSize: 19,
                                        fontWeight: FontWeight.bold)),
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  )),
            );
    });
  }

  _classifyImage(File imageFile) async {
    var classification = await _imageClassifier.classify(imageFile);

    if (classification == null) {
      setState(() {
        _image = null;
        _isLoading = false;
        _isHotDog = null;
      });

      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text("Classification failed, Please try again!")));
    } else {
      setState(() {
        _image = imageFile;
        _isLoading = false;
        _isHotDog = classification.confidence > 0.6;
      });
    }
  }
}
