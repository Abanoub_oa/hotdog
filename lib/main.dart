import 'dart:io' show Platform;
import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hotdog/home_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final cameras = await availableCameras();
  final home = HomeScreen(camera: cameras.first);

  final primary = const Color.fromARGB(100, 230, 150, 39).withOpacity(1.0);
  const onPrimary = Colors.red;
  const background = Colors.white;

  if (Platform.isIOS) {
    runApp(CupertinoApp(
      theme: CupertinoThemeData(
          barBackgroundColor: primary,
          scaffoldBackgroundColor: background,
          primaryColor: primary,
          primaryContrastingColor: onPrimary,
          brightness: Brightness.light),
      home: home,
    ));
  } else {
    runApp(MaterialApp(
      theme: ThemeData(
          colorScheme: ColorScheme(
              background: background,
              brightness: Brightness.light,
              error: Colors.red,
              primary: primary,
              primaryVariant: const Color.fromARGB(100, 199, 104, 28),
              secondary: const Color.fromARGB(100, 209, 26, 24),
              secondaryVariant: const Color.fromARGB(100, 223, 96, 94),
              surface: Colors.white54,
              onSecondary: Colors.brown,
              onError: Colors.white,
              onPrimary: onPrimary,
              onBackground: Colors.black,
              onSurface: Colors.blue)),
      home: home,
    ));
  }
}
